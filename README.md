# gRPC Node.js MasterClass Build Modern API and Microservices


#### 03 006 Why this section

I love to teach frameworks, but first I love to teach the theory behind these frameworks. 

I believe learning the theory is key to understanding the practice. 

You are free to skip this short section if you want to go straight to the code, but hopefully, you find the concepts behind GRPC interesting enough :)

Happy learning!

#### 04 015 Command to Generate code in Protocol Buffers

<p class="lead"><p>Please use the following command to generate code using protobuf plugin. You'll need this in the next lectures.</p>
<pre class="prettyprint linenums">protoc -I=. ./protos/dummy.proto 
  --js_out=import_style=commonjs,binary:./server 
  --grpc_out=./server 
  --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin`
  </pre></p>


#### 05 024 [Exercise] Sum API

<p class="lead"><p><em>Now it is your turn to write code!</em></p><p>In this exercise, your goal is&nbsp;<strong>to implement a Sum RPC&nbsp;Unary API in a CalculatorService:</strong></p><ul><li><p>The function takes a Request message that has two integers, and returns a Response that represents the sum of&nbsp;them.</p></li><li><p>Remember to first implement the service definition in a .proto file, alongside the RPC messages</p></li><li><p>Implement the Server code first</p></li><li><p>Test the server&nbsp;code&nbsp;by implementing the Client</p></li></ul><p><em>Example:</em></p><p>The client will send two numbers&nbsp;(3 and&nbsp;10) and the server will respond with (13)</p><p><strong>Good luck!</strong></p></p>

#### 07 032 [Exercise] PrimeNumberDecomposition API

<p class="lead"><p><em>Now it is your turn to write code!</em></p><p>In this exercise, your goal is&nbsp;<strong>to implement a PrimeNumberDecomposition&nbsp;RPC&nbsp;Server Streaming API in a CalculatorService:</strong></p><ul><li><p>The function takes a Request message that has one&nbsp;integer, and returns a stream of&nbsp;Responses that represent the prime number decomposition of that number (see below for the algorithm).</p></li><li><p>Remember to first implement the service definition in a .proto file, alongside the RPC messages</p></li><li><p>Implement the Server code first</p></li><li><p>Test the server&nbsp;code&nbsp;by implementing the Client</p></li></ul><p><em>Example:</em></p><p>The client will send one number&nbsp;(120) and the server will respond with a stream of&nbsp;(2,2,2,3,5), because&nbsp;<em>120=2*2*2*3*5&nbsp;</em></p><p><em>Algorithm (pseudo code):</em></p><pre class="prettyprint linenums">k = 2
N = 210
while N &gt; 1:
    if N % k == 0:   // if k evenly divides into N
        print k      // this is a factor
        N = N / k    // divide N by k so that we have the rest of the number left.
    else:
        k = k + 1
</pre><p><br></p><p><strong>Good luck!</strong></p></p>

#### 08 039 [Exercise] ComputeAverage API

<p class="lead"><p><em>Now it is your turn to write code!</em></p><p>In this exercise, your goal is&nbsp;<strong>to implement a ComputeAverage RPC&nbsp;Client Streaming API in a CalculatorService:</strong></p><ul><li><p>The function takes a stream of&nbsp;Request message that has one&nbsp;integer, and returns a&nbsp;Response&nbsp;with a double that represents the computed average</p></li><li><p>Remember to first implement the service definition in a .proto file, alongside the RPC messages</p></li><li><p>Implement the Server code first</p></li><li><p>Test the server&nbsp;code&nbsp;by implementing the Client</p></li></ul><p><em>Example:</em></p><p>The client will send a stream of number&nbsp;(1,2,3,4) and the server will respond with (2.5), because&nbsp;<em>(1+2+3+4)/4 = 2.5&nbsp;</em></p><p><strong>Good luck!</strong></p></p>

#### 09 045 [Exercise] FindMaximum API

<p class="lead"><p><em>Now it is your turn to write code!</em></p><p>In this exercise, your goal is&nbsp;<strong>to implement a FindMaximum RPC&nbsp;Bi-Directional Streaming&nbsp;API in a CalculatorService:</strong></p><ul><li><p>The function takes a stream of&nbsp;Request message that has one&nbsp;integer, and returns a stream of&nbsp;Responses that represent the current maximum between all these integers</p></li><li><p>Remember to first implement the service definition in a .proto file, alongside the RPC messages</p></li><li><p>Implement the Server code first</p></li><li><p>Test the server&nbsp;code&nbsp;by implementing the Client</p></li></ul><p><em>Example:</em></p><p>The client will send a stream of number&nbsp;(1,5,3,6,2,20) and the server will respond with a stream of&nbsp;(1,5,6,20)</p><p><strong>Good luck!</strong></p></p>

#### 10 052 Helpful gRPC SSL Node JS Resources

<p class="lead"><p>Here's a link as a reference for node gRPC SSL: <a href="https://github.com/gbahamondezc/node-grpc-ssl" rel="noopener noreferrer" target="_blank">https://github.com/gbahamondezc/node-grpc-ssl</a></p><p>Helpful (non-active git example) example: <a href="https://jsherz.com/grpc/node/nodejs/mutual/authentication/ssl/2017/10/27/grpc-node-with-mutual-auth.html" rel="noopener noreferrer" target="_blank">https://jsherz.com/grpc/node/nodejs/mutual/authentication/ssl/2017/10/27/grpc-node-with-mutual-auth.html</a></p><p>The actual blog post accompanying the previous link: <a href="https://jsherz.com/grpc/node/nodejs/mutual/authentication/ssl/2017/10/27/grpc-node-with-mutual-auth.html" rel="noopener noreferrer" target="_blank">https://jsherz.com/grpc/node/nodejs/mutual/authentication/ssl/2017/10/27/grpc-node-with-mutual-auth.html</a></p><p>Please use these links as guides so you have a little bit of context if you are new to all this. </p><p>I the next lecture, we'll dive in and get a secure channel working.</p><p>See you next!</p></p>
